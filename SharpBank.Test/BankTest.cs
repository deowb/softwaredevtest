﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            AccountFactory factory = new AccountFactory();

            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(factory.CreateAccount(AccountType.CHECKING));

            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0, DateProvider.GetInstance().Now());

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new SavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0, DateProvider.GetInstance().Now());

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0, DateProvider.GetInstance().Now());

            Assert.AreEqual(170.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void GetFirstCustomerShouldNotThrowExceptionIfThereIsAtLeastOneCustomer()
        {
            Bank bank = new Bank();
            Account checkingAccount = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            Assert.DoesNotThrow(() => bank.GetFirstCustomer());
        }

        [Test]
        public void AddCustomerParameterShouldThrowAnExceptionIfCustomerIsNull()
        {
            Bank b = new Bank();
            Assert.Throws(Is.TypeOf<ArgumentException>(), () => b.AddCustomer(null));
        }

        [Test]
        public void TotalInterestPaidShouldReturnTotalInterest()
        {
            Bank b = new Bank();
            Customer cust1 = new Customer("Cust1");
            Customer cust2 = new Customer("Cust1");
            Assert.IsTrue(b.TotalInterestPaid() >= 0);
        }

        [Test]
        public void CustomerSummaryPrintsOnlyTheTitleIfThereAreNoCustomers()
        {
            Bank b = new Bank();
            string summary = b.CustomerSummary();
            Assert.AreEqual(summary, "Customer Summary", "There are no customers");
        }
    }
}
