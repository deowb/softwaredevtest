﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new CheckingAccount();
            Account savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0, DateProvider.GetInstance().Now());
            savingsAccount.Deposit(4000.0, DateProvider.GetInstance().Now());
            savingsAccount.Withdraw(200.0, DateProvider.GetInstance().Now());

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new SavingsAccount());
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());
            oscar.OpenAccount(new MaxiSavingsAccount());
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void OpenAccountShouldReturnCustomer()
        {
            Customer oscar = new Customer("Oscar");
            Assert.IsInstanceOf(typeof(Customer), oscar.OpenAccount(new SavingsAccount()));
        }

        [Test]
        public void GetNameShouldShouldReturnString()
        {
            Customer oscar = new Customer("Oscar");
            Assert.IsInstanceOf(typeof(String), oscar.GetName());
        }

        [Test]
        public void OpenAccountShouldThrowAnExceptionIfAccountIsNull()
        {
            Customer oscar = new Customer("Oscar");
            Assert.Throws(Is.TypeOf<ArgumentException>(), () => oscar.OpenAccount(null));
        }

        [Test]
        public void TotalInterestEarnedShouldReturnADouble()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());
            Assert.IsInstanceOf(typeof(double), oscar.TotalInterestEarned());
        }
    }
}