﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {

        /*
         * Create an account for each account type
         */
        [TestCase(AccountType.CHECKING)]
        [TestCase(AccountType.SAVINGS)]
        [TestCase(AccountType.MAXI_SAVINGS)]
        public void CreateSavingsAccount(AccountType acctType)
        {
            Assert.AreEqual(acctType, CreateAccount(acctType).GetAccountType());
        }

        [TestCase(AccountType.CHECKING)]
        [TestCase(AccountType.SAVINGS)]
        [TestCase(AccountType.MAXI_SAVINGS)]
        public void DepositNegativeAmountShouldThrowArgumentExceptionError(AccountType acctType)
        {
            Assert.Throws(Is.TypeOf<ArgumentException>(), () => CreateAccount(acctType).Deposit(-10, DateProvider.GetInstance().Now()) );
        }

        [TestCase(AccountType.CHECKING)]
        [TestCase(AccountType.SAVINGS)]
        [TestCase(AccountType.MAXI_SAVINGS)]
        public void WithdrawNegativeAmountShouldThrowArgumentExceptionError(AccountType acctType)
        {
            Assert.Throws(Is.TypeOf<ArgumentException>(), () => CreateAccount(acctType).Withdraw(-10, DateProvider.GetInstance().Now()));
        }

        [TestCase(AccountType.CHECKING)]
        [TestCase(AccountType.SAVINGS)]
        [TestCase(AccountType.MAXI_SAVINGS)]
        public void WithdrawResultingInNegativeBalanceShouldThrowArgumentExceptionError(AccountType acctType)
        {
            Account acct = CreateAccount(acctType);
            Assert.Throws(Is.TypeOf<ArgumentException>(),
                () =>
                {
                    acct.transactions = new List<Transaction>();
                    acct.transactions.Add(new Transaction(10, TransactionType.WITHDRAWAL, DateProvider.GetInstance().Now(), acct));
                    acct.Withdraw(-20, DateProvider.GetInstance().Now());
                });
        }

        [TestCase(AccountType.CHECKING, 500)]
        [TestCase(AccountType.CHECKING, 1500)]
        [TestCase(AccountType.SAVINGS, 500)]
        [TestCase(AccountType.SAVINGS, 1500)]
        [TestCase(AccountType.MAXI_SAVINGS, 500)]
        [TestCase(AccountType.MAXI_SAVINGS, 2500)]
        public void InterestEarned(AccountType acctType, double deposit)
        {
            Account acct = CreateAccount(acctType);
            acct.Deposit(deposit, DateProvider.GetInstance().Now());

            double interest = acct.InterestEarned();

            Assert.That(interest, Is.TypeOf<double>());
        }

        [Test]
        public void TransferToShouldThrowAnErrorIfAmountBeingTransferredIsGreaterThanTheBalance()
        {
            Assert.Throws(Is.TypeOf<ArgumentException>(),
                () =>
                {
                    Account savings = new SavingsAccount();
                    Account checking = new CheckingAccount();
                    savings.Deposit(100, DateProvider.GetInstance().Now());
                    checking.Deposit(200, DateProvider.GetInstance().Now());
                    checking.Deposit(100, DateProvider.GetInstance().Now());

                    checking.TransferTo(savings, 350);
                });
        }

        [TestCase(AccountType.CHECKING)]
        [TestCase(AccountType.SAVINGS)]
        [TestCase(AccountType.MAXI_SAVINGS)]
        public void GetAccountType(AccountType acctType)
        {
            Account acct = CreateAccount(acctType);
            Assert.AreEqual(typeof(AccountType).UnderlyingSystemType.Name, "AccountType");
        }


        private Account CreateAccount(AccountType acctType)
        {
            Account acct = null;
            if (acctType == AccountType.CHECKING)
            {
                acct = new CheckingAccount();
            }
            else if (acctType == AccountType.SAVINGS)
            {
                acct = new SavingsAccount();
            }
            else
            {
                acct = new MaxiSavingsAccount();
            }

            return acct;

        }

    }
}