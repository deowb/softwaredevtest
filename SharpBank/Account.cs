﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public abstract class Account : IAccount
    {
        private readonly object _transactionLock = new object();

        public List<Transaction> transactions;
        public readonly DateTime dateOpened;

        public Account()
        {
            this.transactions = new List<Transaction>();
            this.dateOpened = DateProvider.GetInstance().Now();
        }

        public void Deposit(double amount, DateTime transactionDate)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Deposit amount must be greater than zero");
            }
            lock (_transactionLock)
            {
                transactions.Add(new Transaction(amount, TransactionType.DEPOSIT, transactionDate, this));
            }
        }

        public void Withdraw(double amount, DateTime transactionDate)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Withdrawal amount must be greater than zero");
            }

            if (this.SumTransactions() - amount < 0)
            {
                throw new ArgumentException("Withdrawal exceeds total amount in account resulting in negative balance");
            }

            lock (_transactionLock)
            {
                transactions.Add(new Transaction(-amount, TransactionType.WITHDRAWAL, DateProvider.GetInstance().Now(), this));
            }
        }

        public abstract double InterestEarned();

        public virtual double SumTransactions()
        {
            return transactions.Sum(t => t.amount);
        }

        public void TransferTo(Account transferTo, double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Transfer amount must be greater than zero");
            }
            if (this.SumTransactions() > amount)
            {
                throw new ArgumentException("Insufficient funds. Amount being trasnferred is greater than the balance.");
            }

            this.Withdraw(amount, DateProvider.GetInstance().Now());
            transferTo.Deposit(amount, DateProvider.GetInstance().Now());
        }


        public abstract AccountType GetAccountType();
    }
}
