﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;
        public readonly TransactionType transactionType;
        public readonly Account account;

        public readonly DateTime transactionDate;

        public Transaction(double amount, TransactionType transactionType, DateTime transactionDate, Account account)
        {
            this.amount = amount;
            this.transactionType = transactionType;
            this.account = account;
            this.transactionDate = transactionDate;
        }

    }
}
