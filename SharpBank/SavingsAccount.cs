﻿namespace SharpBank
{
    public class SavingsAccount : Account
    {
        public sealed override double InterestEarned()
        {
            double amount = base.SumTransactions();
            return (amount <= 1000) ? amount * Constants.InterestRates.SavingsAccount1KRate : 1 + (amount - 1000) * Constants.InterestRates.SavingsAccount1KPlusRate;
        }

        public sealed override AccountType GetAccountType()
        {
            return AccountType.SAVINGS;
        }
    }
}
