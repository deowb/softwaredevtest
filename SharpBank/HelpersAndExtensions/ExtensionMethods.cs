﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class ExtensionMethods
    {
        public static string ToDollarString(this double amount)
        {
            return String.Format("${0:N2}", Math.Abs(amount));
        }

        public static string ToDisplayName(this Enum en)
        {
            var attrs = en.GetType()
                        .GetMember(en.ToString())
                        .Single()
                        .GetCustomAttributes(typeof(DisplayAttribute), false);
            return ((DisplayAttribute)attrs[0]).Name;
        }
    }
}