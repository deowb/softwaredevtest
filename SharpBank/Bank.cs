﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Bank : IBank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentException("Customer should not be null");
            customers.Add(customer);
        }

        public String CustomerSummary()
        {
            String summary = "Customer Summary";
            customers.ForEach(
                c => summary += "\n - " + c.GetName() + " (" + Helpers.PluralOrSingular(c.GetNumberOfAccounts(), "account") + ")");
            return summary;
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            customers.ForEach(c => total += c.TotalInterestEarned());
            return total;
        }

        public String GetFirstCustomer()
        {
            try
            {
                customers = null;
                return customers[0].GetName();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }
    }
}
