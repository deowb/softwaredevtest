﻿namespace SharpBank
{
    public class CheckingAccount : Account
    {
        public sealed override double InterestEarned()
        {
            return base.SumTransactions() * Constants.InterestRates.CheckingAccountRate;
        }

        public sealed override AccountType GetAccountType()
        {
            return AccountType.CHECKING;
        }
    }
}
