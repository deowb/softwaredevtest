﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer : ICustomer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            if (account == null)
                throw new ArgumentException("Account should not be null");
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            accounts.ForEach(a => total += a.InterestEarned());
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            String statement = null;
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + total.ToDollarString();
            return statement;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";
            s = a.GetAccountType().ToDisplayName() + " Account\n";

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + t.amount.ToDollarString() + "\n";
                total += t.amount;
            }
            s += "Total " + total.ToDollarString();
            return s;
        }

    }
}
