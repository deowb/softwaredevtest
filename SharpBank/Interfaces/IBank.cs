﻿namespace SharpBank
{
    public interface IBank
    {
        void AddCustomer(Customer customer);
        string CustomerSummary();
        string GetFirstCustomer();
        double TotalInterestPaid();
    }
}