﻿using System;

namespace SharpBank
{
    public interface IAccount
    {
        void Deposit(double amount, DateTime transactionDate);
        AccountType GetAccountType();
        double InterestEarned();
        void Withdraw(double amount, DateTime transactionDate);
    }
}