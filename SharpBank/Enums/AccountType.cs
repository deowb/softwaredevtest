﻿using System.ComponentModel.DataAnnotations;
namespace SharpBank
{
    public enum AccountType
    {
        [Display(Name = "Checking")]
        CHECKING = 1,
        [Display(Name = "Savings")]
        SAVINGS,
        [Display(Name = "Maxi Savings")]
        MAXI_SAVINGS
    }
}