﻿
namespace SharpBank
{
    public enum TransactionType
    {
        DEPOSIT = 1,
        WITHDRAWAL,
        INTEREST
    }
}