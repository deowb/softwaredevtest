﻿using System;
using System.Linq;

namespace SharpBank
{
    public class MaxiSavingsAccount : Account
    {
        public sealed override double InterestEarned()
        {
            double amount = base.SumTransactions();
            if (amount <= 1000)
                return amount * Constants.InterestRates.MaxiSavingsAccount1KRate;
            if (amount <= 2000)
                return 20 + (amount - 1000) * Constants.InterestRates.MaxiSavingsAccount2KRate;

            return 70 + (amount - 2000) * (EligibleForHigherInterest() ? Constants.InterestRates.MaxiSavingsAccount2KPlusRate : Constants.InterestRates.MaxiSavingsPrimeRate);
        }

        public sealed override AccountType GetAccountType()
        {
            return AccountType.MAXI_SAVINGS;
        }

        internal bool EligibleForHigherInterest()
        {
            DateTime now = DateProvider.GetInstance().Now();
            return transactions.Any(t => t.transactionType == TransactionType.WITHDRAWAL
                                    && t.account.GetAccountType() == AccountType.MAXI_SAVINGS
                                    && now.Subtract(t.transactionDate).TotalDays < 10);
        }
    }
}
