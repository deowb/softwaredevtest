﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class Constants
    {
        public static class InterestRates
        {
            public const double CheckingAccountRate = .001;

            public const double SavingsAccount1KRate = .001;
            public const double SavingsAccount1KPlusRate = .002;

            public const double MaxiSavingsAccount1KRate = .001;
            public const double MaxiSavingsAccount2KRate = .02;
            public const double MaxiSavingsAccount2KPlusRate = .05;
            public const double MaxiSavingsPrimeRate = .1;
        }
    }
}
