﻿using System;

namespace SharpBank
{
    public class AccountFactory
    {
        public Account CreateAccount(AccountType accountType)
        {
            switch (accountType)
            {
                case AccountType.SAVINGS:
                    { return new SavingsAccount(); }
                case AccountType.CHECKING:
                    { return new CheckingAccount(); }
                case AccountType.MAXI_SAVINGS:
                    { return new MaxiSavingsAccount(); }
                default:
                    throw new ArgumentException("No such account type exists");
            }
        }
    }
}
